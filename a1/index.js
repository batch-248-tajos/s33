const url = 'https://jsonplaceholder.typicode.com/todos'

/**
 * //1
3. Create a fetch request using the GET method that will retrieve all the to 
    do list items from JSON Placeholder API.
4. Using the data retrieved, create an array using the map method to return 
    just the title of every item and print the result in the console.
 */

const num1 = async () => {
    const res = await fetch(url)
    const data = await res.json()

    const newData = data.map((item) => {
        return item.title
    })
    console.log(newData)
}

/**
 * //2
5. Create a fetch request using the GET method that will retrieve a single 
    to do list item from JSON Placeholder API.
6. Using the data retrieved, print a message in the console that will provide 
    the title and status of the to do list item.
 */
const num2 = async () => {
    const res = await fetch(url + '/1')
    const data = await res.json()
    console.log(`
        The item "${data.title}" on the list has a status of ${data.completed}`
    )
}

/**
 * //3
7. Create a fetch request using the POST method that will create a to do list 
    item using the JSON Placeholder API.
 */
const num3 = async () => {
    const res = await fetch(url, {
        method: 'POST',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
            title: 'Created To Do List Item',
            completed: false,
            userId: 1,
            id: 201
        })
    })
    const data = await res.json()
    console.log(data)
}

/**
 * //4
    8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
    9. Update a to do list item by changing the data structure to contain the following properties:
    - Title
    - Description
    - Status
    - Date Completed
    - User ID
 */
const num4 = async () => {
    const res = await fetch(url + '/1', {
        method: 'PUT',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
            title: 'Update TO Do List Item',
            description: 'lorem epsilum',
            status: 'Pending',
            dateCompleted: 'Pending',
            userId: 1
        })
    })
    const data = await res.json()
    console.log(data)
}

/**
 * //5
    10. Create a fetch request using the PATCH method that will update a to do list 
        item using the JSON Placeholder API.
    11. Update a to do list item by changing the status to complete and add a 
        date when the status was changed.
 */
const num5 = async () => {
    const res = await fetch(url + '/1', {
        method: 'PATCH',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
            status: 'Complete',
            dateCompleted: '02/22/2023'
        })
    })
    const data = await res.json()
    console.log(data)
}

/**
 * //6
    12. Create a fetch request using the DELETE method that will delete an item 
    using the JSON Placeholder API.

 */
const num6 = async () => {
    const res = await fetch(url + '/1', {
        method: 'DELETE'
    })
    const data = await res.json()
    console.log(data)
}

const run = async() => {
    await num1()
    await num2()
    await num3()
    await num4()
    await num5()
    await num6()
}

run()